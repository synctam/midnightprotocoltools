@rem
@rem Midnight Protocol アセット抽出用バッチファイル
@rem
@SET PATH=tools;%PATH%
@echo アセットの抽出はオリジナルのリソースファイルが必要です。
@echo Steam の「ゲームファイルの整合性を確認」を実行し、リソースファイルを最新の状態にしてください。
@choice /m "処理を続行しますか？"
@if %errorlevel% equ 1 goto :execute
@goto :break

:execute
MpParaTranzCli.exe ^
	--export ^
	-i ..\MidnightProtocol_Data\resources.assets ^
	-o data\en\resources_00001.-86 ^
	--tpk data\config\classdata.tpk ^
	--asset I2Languages ^
	-r
@if errorlevel 1 goto :errorend


@echo ********
@echo 正常終了
@echo ********
@goto :end

:break
@echo ******************************
@echo 処理を中止しました。
@echo ******************************
@goto :end

:errorend
@echo ******************************
@echo エラーのため処理を中止しました。
@echo ******************************

:end



@pause
@exit /b
