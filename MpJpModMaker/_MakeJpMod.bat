@rem
@rem Midnight Protocol 日本語化ＭＯＤ作成用バッチファイル
@rem

@SET PATH=tools;%PATH%
@SET RES=resources_00001.-86
@SET TYPE=-86

MpParaTranzCli.exe ^
	--download ^
	--url https://paratranz.cn/api/projects/3302/artifacts/download ^
	-f data\ParaTranz
@if errorlevel 1 goto :errorend

MpParaTranzCli.exe ^
	--makemod ^
	-l 2 ^
	-i data\en\%RES% ^
	-o data\jp\%RES% ^
	-p data\ParaTranz\raw\MpParaTrans_vanilla.json.json ^
	--usenbsp ^
	-r
@if errorlevel 1 goto :errorend

@SET EXDIR=..\MidnightProtocol_Data\Unity_Assets_Files\resources\
@if exist %EXDIR% goto cp1
@mkdir %EXDIR%
@if errorlevel 1 goto :errorend

:cp1
copy /y data\jp\%RES% ..\MidnightProtocol_Data\Unity_Assets_Files\resources\%RES%
@if errorlevel 1 goto :errorend

@cd ..\MidnightProtocol_Data
@if errorlevel 1 goto :errorend

..\MpJpModMaker\tools\UnityEX import resources.assets -t %TYPE%
@if errorlevel 1 goto :errorend

@echo ********
@echo 正常終了
@echo ********
@goto :end


:errorend
@echo ******************************
@echo エラーのため処理を中止しました。
@echo ******************************
:end


@pause
@exit /b
