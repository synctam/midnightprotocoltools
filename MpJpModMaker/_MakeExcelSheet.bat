@rem
@rem Midnight Protocol 翻訳シート出力用バッチファイル
@rem

@SET PATH=tools;%PATH%
@SET RES=resources_00001.-86

MpParaTranzCli.exe ^
	--makesheet ^
	-l 2 ^
	-i data\jp\%RES% ^
	-m data\excel\Midnight_Protocol_Localization_External.xlsx ^
	-x _MpTransSheet(beta).xlsx ^
	-r
@if errorlevel 1 goto :errorend


@echo ********
@echo 正常終了
@echo ********
@goto :end


:errorend
@echo ******************************
@echo エラーのため処理を中止しました。
@echo ******************************
:end


@pause
@exit /b
