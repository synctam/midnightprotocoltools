﻿namespace LibMpTrans.ResourceObjects
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using LibMpTrans.LanguageDara;
    using LibMpTrans.ParaTranz;
    using LibMpTrans.StringUtils;
    using ParaTransData;

    public class MpResourceDao
    {
        public static void SaveToFile(
            MpParaTranzDataFile paraTranzDataFile,
            string sourcePath,
            string targetPath,
            int langNo,
            bool useNobreakSpace)
        {
            using (var sr = new StreamReader(sourcePath))
            using (var sw = new StreamWriter(targetPath, false))
            {
                var br = new BinaryReader(sr.BaseStream, Encoding.UTF8);
                var bw = new BinaryWriter(sw.BaseStream, Encoding.UTF8);

                var m_GameObject_m_FileID = br.ReadInt32();
                bw.Write(m_GameObject_m_FileID);

                var m_GameObject_m_PathID = br.ReadInt64();
                bw.Write(m_GameObject_m_PathID);
                var m_Enabled = SyBinaryUtils.ReadBoolean(br);
                SyBinaryUtils.WriteBoolean(bw, m_Enabled);

                var m_Script_m_FileID = br.ReadInt32();
                bw.Write(m_Script_m_FileID);

                var m_Script_m_PathID = br.ReadInt64();
                bw.Write(m_Script_m_PathID);

                var m_Name = SyBinaryUtils.ReadString(br);
                SyBinaryUtils.WriteString(bw, m_Name);

                var mSource_UserAgreesToHaveItOnTheScene = SyBinaryUtils.ReadBoolean(br);
                SyBinaryUtils.WriteBoolean(bw, mSource_UserAgreesToHaveItOnTheScene);

                var mSource_UserAgreesToHaveItInsideThePluginsFolder = SyBinaryUtils.ReadBoolean(br);
                SyBinaryUtils.WriteBoolean(bw, mSource_UserAgreesToHaveItInsideThePluginsFolder);

                var mSource_GoogleLiveSyncIsUptoDate = SyBinaryUtils.ReadBoolean(br);
                SyBinaryUtils.WriteBoolean(bw, mSource_GoogleLiveSyncIsUptoDate);

                var mTerms_count = br.ReadInt32();
                bw.Write(mTerms_count);

                for (int itemPos = 0; itemPos < mTerms_count; itemPos++)
                {
                    var term = SyBinaryUtils.ReadString(br);
                    if (term.Equals("Databit/KillingPavo/Content"))
                    {
                        Console.WriteLine();
                    }

                    SyBinaryUtils.WriteString(bw, term);

                    var termType = br.ReadInt32();
                    bw.Write(termType);

                    var languagesCount = br.ReadInt32();
                    bw.Write(languagesCount);

                    var originalText = string.Empty;
                    for (int langPos = 0; langPos < languagesCount; langPos++)
                    {
                        var languageText = SyBinaryUtils.ReadString(br);
                        if (langPos == 0)
                        {
                            //// 英語の原文を保管
                            originalText = languageText;
                        }

                        if (langPos == langNo)
                        {
                            //// 翻訳対象の言語番号の時は、翻訳テキストを転記する。
                            var translatedEntry = paraTranzDataFile.GetItem(term) as MpParaTranzDataEntry;
                            var translatedText = translatedEntry?.Translation;
                            if (string.IsNullOrWhiteSpace(translatedText))
                            {
                                //// 未翻訳の場合は英語の原文を転記する。
                                translatedText = originalText;
                            }

                            if (term.Equals("Intranet/IntranetCat/Result0"))
                            {
                                //// "Intranet/IntranetCat/Result0" の "\\n" は ASCII ART
                                //// "Hardware/TraceModifierDescription" が原文も "\\n" になっているが、
                                //// これはゲームのバグと思われるので除外した。
                            }
                            else
                            {
                                //// [暫定対応] 日本語化MOD作成ツールのバグ回避
                                translatedText = Sanitaize(translatedText);
                                translatedText = ChangeNbspTag(translatedText, useNobreakSpace);
                            }

                            SyBinaryUtils.WriteString(bw, translatedText);
                        }
                        else
                        {
                            //// 翻訳対象外の言語番号の時は、原文を転機する。
                            SyBinaryUtils.WriteString(bw, languageText);
                        }
                    }

                    var flagCount = br.ReadInt32();
                    bw.Write(flagCount);

                    for (int flagPos = 0; flagPos < flagCount; flagPos++)
                    {
                        var flag = br.ReadByte();
                        bw.Write(flag);
                    }

                    SyBinaryUtils.SkipPadding(br);
                    SyBinaryUtils.SkipPadding(bw);

                    var languages_Touch_Count = br.ReadInt32();
                    bw.Write(languages_Touch_Count);
                    for (var languagesTouchPos = 0; languagesTouchPos < languages_Touch_Count; languagesTouchPos++)
                    {
                        Console.WriteLine($"*** NotImplementedException ***");
                    }
                }

                var caseInsensitiveTerms = SyBinaryUtils.ReadBoolean(br);
                SyBinaryUtils.WriteBoolean(bw, caseInsensitiveTerms);

                var onMissingTranslation = br.ReadInt32();
                bw.Write(onMissingTranslation);

                var mTerm_AppName = SyBinaryUtils.ReadString(br);
                SyBinaryUtils.WriteString(bw, mTerm_AppName);

                var mLanguagesCount = br.ReadInt32();
                bw.Write(mLanguagesCount);

                for (var langPos = 0; langPos < mLanguagesCount; langPos++)
                {
                    var name = SyBinaryUtils.ReadString(br);
                    SyBinaryUtils.WriteString(bw, name);

                    var code = SyBinaryUtils.ReadString(br);
                    SyBinaryUtils.WriteString(bw, code);

                    var flag = br.ReadInt32();
                    bw.Write(flag);
                }

                var ignoreDeviceLanguage = SyBinaryUtils.ReadBoolean(br);
                SyBinaryUtils.WriteBoolean(bw, ignoreDeviceLanguage);

                var allowUnloadingLanguages = br.ReadInt32();
                bw.Write(allowUnloadingLanguages);

                var google_WebServiceURL = SyBinaryUtils.ReadString(br);
                SyBinaryUtils.WriteString(bw, google_WebServiceURL);

                var google_SpreadsheetKey = SyBinaryUtils.ReadString(br);
                SyBinaryUtils.WriteString(bw, google_SpreadsheetKey);

                var google_SpreadsheetName = SyBinaryUtils.ReadString(br);
                SyBinaryUtils.WriteString(bw, google_SpreadsheetName);

                var google_LastUpdatedVersion = SyBinaryUtils.ReadString(br);
                SyBinaryUtils.WriteString(bw, google_LastUpdatedVersion);

                var googleUpdateFrequency = br.ReadInt32();
                bw.Write(googleUpdateFrequency);

                var googleInEditorCheckFrequency = br.ReadInt32();
                bw.Write(googleInEditorCheckFrequency);

                var googleUpdateSynchronization = br.ReadInt32();
                bw.Write(googleUpdateSynchronization);

                var googleUpdateDelay = br.ReadSingle();
                bw.Write(googleUpdateDelay);

                var assetsCount = br.ReadInt32();
                bw.Write(assetsCount);

                for (var assetsPos = 0; assetsPos < assetsCount; assetsPos++)
                {
                    Console.WriteLine($"*** NotImplementedException ***");
                }

                if (br.BaseStream.Length > br.BaseStream.Position)
                {
                    throw new Exception($"{br.BaseStream.Length - br.BaseStream.Position}バイトの未読み込みのデータが存在します。");
                }
                else
                {
                    //// すべてのデータを正常に書き込んだ。
                }
            }
        }

        public static void LoadFromFile(LanguageDataInfo languageDataInfo, string path)
        {
            using (var sr = new StreamReader(path, Encoding.UTF8))
            using (var br = new BinaryReader(sr.BaseStream, Encoding.UTF8))
            {
                var m_GameObject_m_FileID = br.ReadInt32();
                var m_GameObject_m_PathID = br.ReadInt64();

                var m_Enabled = SyBinaryUtils.ReadBoolean(br);

                var m_Script_m_FileID = br.ReadInt32();
                var m_Script_m_PathID = br.ReadInt64();

                var m_Name = SyBinaryUtils.ReadString(br);
                var mSource_UserAgreesToHaveItOnTheScene = SyBinaryUtils.ReadBoolean(br);
                var mSource_UserAgreesToHaveItInsideThePluginsFolder = SyBinaryUtils.ReadBoolean(br);
                var mSource_GoogleLiveSyncIsUptoDate = SyBinaryUtils.ReadBoolean(br);

                var mTerms_count = br.ReadInt32();
                for (int itemPos = 0; itemPos < mTerms_count; itemPos++)
                {
                    var term = SyBinaryUtils.ReadString(br);
                    var termType = br.ReadInt32();
                    var languagesCount = br.ReadInt32();
                    for (int langPos = 0; langPos < languagesCount; langPos++)
                    {
                        var languageDataFile = languageDataInfo.GetLanguageFile(langPos);
                        if (languageDataFile == null)
                        {
                            languageDataFile = new LanguageDataFile(langPos);
                            languageDataInfo.AddFile(languageDataFile);
                        }

                        var languageText = SyBinaryUtils.ReadString(br);
                        //// エントリーの作成
                        var languageDataEntry = new LanguageDataEntry(term, languageText, null);
                        languageDataFile.AddEntry(languageDataEntry);
                    }

                    var flagCount = br.ReadInt32();
                    for (int flagPos = 0; flagPos < flagCount; flagPos++)
                    {
                        var flag = br.ReadByte();
                    }

                    SyBinaryUtils.SkipPadding(br);

                    var languages_Touch_Count = br.ReadInt32();
                    for (var languagesTouchPos = 0; languagesTouchPos < languages_Touch_Count; languagesTouchPos++)
                    {
                        Console.WriteLine($"*** NotImplementedException ***");
                    }
                }

                var caseInsensitiveTerms = SyBinaryUtils.ReadBoolean(br);
                var onMissingTranslation = br.ReadInt32();
                var mTerm_AppName = SyBinaryUtils.ReadString(br);

                var mLanguagesCount = br.ReadInt32();
                for (var langPos = 0; langPos < mLanguagesCount; langPos++)
                {
                    var name = SyBinaryUtils.ReadString(br);
                    var code = SyBinaryUtils.ReadString(br);
                    var flag = br.ReadInt32();
                }

                var ignoreDeviceLanguage = SyBinaryUtils.ReadBoolean(br);
                var allowUnloadingLanguages = br.ReadInt32();
                var google_WebServiceURL = SyBinaryUtils.ReadString(br);
                var google_SpreadsheetKey = SyBinaryUtils.ReadString(br);
                var google_SpreadsheetName = SyBinaryUtils.ReadString(br);
                var google_LastUpdatedVersion = SyBinaryUtils.ReadString(br);
                var googleUpdateFrequency = br.ReadInt32();
                var googleInEditorCheckFrequency = br.ReadInt32();
                var googleUpdateSynchronization = br.ReadInt32();
                var googleUpdateDelay = br.ReadSingle();

                var assetsCount = br.ReadInt32();
                for (var assetsPos = 0; assetsPos < assetsCount; assetsPos++)
                {
                    Console.WriteLine($"*** NotImplementedException ***");
                }

                if (br.BaseStream.Length > br.BaseStream.Position)
                {
                    throw new Exception($"{br.BaseStream.Length - br.BaseStream.Position}バイトの未読み込みのデータが存在します。");
                }
                else
                {
                    //// すべてのデータを正常に読み込んだ。
                }
            }
        }

        /// <summary>
        /// タグ<nbsp>を文字列に変換したテキストを返す。
        /// </summary>
        /// <param name="text">テキスト</param>
        /// <param name="useNobreakSpace">ノーブレークスペースの使用有無</param>
        /// <returns>文字列に変換したテキスト</returns>
        private static string ChangeNbspTag(string text, bool useNobreakSpace)
        {
            var buff = new StringBuilder(text);
            if (useNobreakSpace)
            {
                buff.Replace("<nbsp>", "\u00A0");
            }
            else
            {
                //// ノーブレークスペースを使用しない場合は通常の半角スペースに置換する。
                buff.Replace("<nbsp>", " ");
            }

            return buff.ToString();
        }

        private static string Sanitaize(string text)
        {
            var buff = new StringBuilder(text);

            buff.Replace("\\n", "\n");

            return buff.ToString();
        }
    }
}
