﻿namespace LibMpTrans.ExcelSheets
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ClosedXML.Excel;
    using LibMpTrans.LanguageDara;

    public class MpExcelSheetDao
    {
        public static void SaveToFile(
            LanguageDataInfo languageDataInfo, string masterSheetPath, string path, int langNo)
        {
            using (var workbook = new XLWorkbook(masterSheetPath))
            {
                var oldFileType = string.Empty;
                foreach (var worksheet in workbook.Worksheets)
                {
                    Console.WriteLine($"Sheet({worksheet.Name})");
                    var usedRange = worksheet.RangeUsed();
                    foreach (var row in usedRange.Rows())
                    {
                        var key = row.Cell(1).Value.ToString();
                        if (key.Equals("Keys", StringComparison.OrdinalIgnoreCase))
                        {
                            continue;
                        }

                        var fullKey = string.Empty;
                        if (worksheet.Name.Equals("Default", StringComparison.OrdinalIgnoreCase))
                        {
                            //// 公式翻訳シートのシート名"Default"の場合は、キーにシート名を付加しない。
                            fullKey = key;
                        }
                        else
                        {
                            //// キーにシート名を付加する。
                            fullKey = $"{worksheet.Name}/{key}";
                        }

                        var translatedEntry = languageDataInfo.GetEntry(langNo, fullKey);
                        if (translatedEntry == null)
                        {
                            Console.WriteLine($"\tKey({key}) **** Translation data does not exist. ****");
                        }
                        else
                        {
                            row.Cell(7).Value = translatedEntry.Text;
                        }
                    }
                }

                workbook.SaveAs(path);
            }
        }

        public static void SaveToFile(LanguageDataInfo languageDataInfo, string path, int langNo)
        {
            using (var workbook = new XLWorkbook())
            {
                var oldFileType = string.Empty;
                var worksheet = workbook.Worksheets.Add("vanilla");
                var rowNo = 1;

                var columnNo = 1;

                //// ヘッダー出力
                worksheet.Cell(rowNo, columnNo).Value = "Keys";
                worksheet.Column(columnNo).Width = 50;
                columnNo++;

                worksheet.Cell(rowNo, columnNo).Value = "English";
                worksheet.Column(columnNo).Width = 70;
                columnNo++;

                worksheet.Cell(rowNo, columnNo).Value = "Japanese";
                worksheet.Column(columnNo).Width = 70;
                columnNo++;

                rowNo++;

                foreach (var languageDataFile in languageDataInfo.Items.Values)
                {
                    if (languageDataFile.LanguageNo == 0)
                    {
                        foreach (var languageDataEntry in languageDataFile.Items.Values)
                        {
                            columnNo = 1;
                            var key = languageDataEntry.Key;
                            var englishText = languageDataEntry.Text;
                            var jpEntry = languageDataInfo.GetEntry(langNo, languageDataEntry.Key);
                            var jpText = jpEntry?.Text.Replace(@"\n", "\n");
                            if (englishText.Equals(jpText))
                            {
                                //// 原文と同一の場合はクリアする。
                                jpText = null;
                            }

                            worksheet.Cell(rowNo, columnNo).Value = key;
                            worksheet.Cell(rowNo, columnNo).Style.Alignment.WrapText = true;
                            worksheet.Cell(rowNo, columnNo).Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                            columnNo++;

                            worksheet.Cell(rowNo, columnNo).Value = englishText;
                            worksheet.Cell(rowNo, columnNo).Style.Alignment.WrapText = true;
                            worksheet.Cell(rowNo, columnNo).Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                            columnNo++;

                            worksheet.Cell(rowNo, columnNo).Value = jpText;
                            worksheet.Cell(rowNo, columnNo).Style.Alignment.WrapText = true;
                            worksheet.Cell(rowNo, columnNo).Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                            columnNo++;

                            //// 行の高さを自動調整する
                            worksheet.Row(1).AdjustToContents();

                            rowNo++;
                        }
                    }
                }

                worksheet.SheetView.Freeze(1, 0);

                workbook.SaveAs(path);
            }
        }
    }
}
