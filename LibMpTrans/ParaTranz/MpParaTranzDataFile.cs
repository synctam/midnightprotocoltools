﻿namespace LibMpTrans.ParaTranz
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ParaTransData;

    public class MpParaTranzDataFile
    {
        /// <summary>
        /// ToDo: デモ版、正規版共にキーの大文字小文字を区別する項目が２箇所ある。
        /// 1.Intranet/Intranetenra/Result0, Intranet/IntranetENRA/Result0
        /// 2.Intranet/Intranetenra/Terms, Intranet/IntranetENRA/Terms
        /// キーは term
        /// </summary>
        public Dictionary<string, TrParaTranzDataWidthStatusSchema> Items { get; } =
            new Dictionary<string, TrParaTranzDataWidthStatusSchema>();

        /// <summary>
        /// 辞書をリストに変換して返す。
        /// </summary>
        /// <returns>リスト化した辞書</returns>
        public List<TrParaTranzDataWidthStatusSchema> ToList()
        {
            var result = new List<TrParaTranzDataWidthStatusSchema>();

            foreach (var item in this.Items.Values)
            {
                result.Add(item);
            }

            return result;
        }

        /// <summary>
        /// 辞書から翻訳済みテキストを抽出し、リスト形式で返す。
        /// </summary>
        /// <returns>本宅済みテキストのリスト</returns>
        public List<TrParaTranzDataWidthStatusSchema> ToTranslatedList()
        {
            var result = new List<TrParaTranzDataWidthStatusSchema>();

            foreach (var item in this.Items.Values)
            {
                if (item.Original.Equals(item.Translation, StringComparison.OrdinalIgnoreCase))
                {
                    //// 未翻訳データは処理しない
                }
                else
                {
                    //// 翻訳済みデータを追加する。
                    result.Add(item);
                }
            }

            return result;
        }

        /// <summary>
        /// アイテムを追加する。キー重複時は例外が発生する。
        /// </summary>
        /// <param name="paraTranzDataWidthStatusSchema">アイテム</param>
        public void AddItem(TrParaTranzDataWidthStatusSchema paraTranzDataWidthStatusSchema)
        {
            if (this.Items.TryAdd(paraTranzDataWidthStatusSchema.Key, paraTranzDataWidthStatusSchema))
            {
                //// OK
            }
            else
            {
                var msg = $"The key already exists. Key({paraTranzDataWidthStatusSchema.Key})";
                throw new Exception(msg);
            }
        }

        public override string ToString()
        {
            var buff = new StringBuilder();

            foreach (var item in this.Items.Values)
            {
                buff.Append(item.ToString());
            }

            return buff.ToString();
        }

        public TrParaTranzDataWidthStatusSchema GetItem(string key)
        {
            if (this.Items.ContainsKey(key))
            {
                var result = this.Items[key];
                return result;
            }
            else
            {
                return null;
            }
        }
    }
}
