﻿namespace LibMpTrans.ParaTranz
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Text;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class MpParaTranzDao
    {
        public static void LoadFromFile(MpParaTranzDataFile paraTranzDataFile, string path)
        {
            var json = File.ReadAllText(path, Encoding.UTF8);
            var items = JsonConvert.DeserializeObject<List<MpParaTranzDataEntry>>(
                  json, TrParaTranzRawConverter.Settings);
            foreach (var item in items)
            {
                paraTranzDataFile.AddItem(item);
            }
        }

        internal static class TrParaTranzRawConverter
        {
            public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
            {
                MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                DateParseHandling = DateParseHandling.None,
                Formatting = Formatting.Indented,
                Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal },
            },
            };
        }
    }
}
