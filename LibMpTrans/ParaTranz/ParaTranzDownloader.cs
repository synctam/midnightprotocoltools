﻿namespace LibMpTrans.ParaTranz
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.IO.Compression;
    using System.Net;
    using System.Text;

    public class ParaTranzDownloader
    {
        /// <summary>
        /// ParaTranz の APIキー
        /// </summary>
        private readonly string apiKey = "f0e0c71eecc795af7450424814613c8c";

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="apiKey">APIキー</param>
        public ParaTranzDownloader() { }

        /// <summary>
        /// ParaTranzから翻訳データをダウンロード、解凍、ダウンロードファイルの削除を行う。
        /// </summary>
        /// <param name="uri">ダウンロードするＵＲＬ</param>
        /// <param name="folderPath">ダウンロードするフォルダーのパス</param>
        public void Downlaod(Uri uri, string folderPath)
        {
            using (var wc = new WebClient())
            {
                var tempFileName = Path.GetTempFileName();
                var zipFilePath = Path.Combine(folderPath, tempFileName);
                try
                {
                    //// ダウンロード
                    Console.WriteLine($"downloading...");
                    wc.Headers.Add("Authorization", this.apiKey);
                    wc.DownloadFile(uri.AbsoluteUri, tempFileName);
                    //// 解凍
                    Console.WriteLine($"Unzipping...");
                    ZipFile.ExtractToDirectory(zipFilePath, folderPath, Encoding.UTF8, overwriteFiles: true);
                }
                finally
                {
                    //// ダウンロードしたファイルを削除
                    if (File.Exists(zipFilePath))
                    {
                        File.Delete(zipFilePath);
                    }
                }
            }
        }
    }
}
