﻿namespace LibMpTrans.ParaTranz
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Newtonsoft.Json;
    using ParaTransData;

    public class MpParaTranzDataEntry : TrParaTranzDataWidthStatusSchema
    {
        [JsonProperty("stage")]
        public int Stage { get; set; }

        public override string ToString()
        {
            var buff = new StringBuilder();

            buff.AppendLine($"Key({this.Key}) Stage({this.Stage}) Text({this.Original})");

            return buff.ToString();
        }
    }
}
