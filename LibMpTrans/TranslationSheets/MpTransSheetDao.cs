﻿namespace LibMpTrans.TranslationSheets
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Text;
    using CsvHelper;
    using CsvHelper.Configuration;
    using LibMpTrans.ParaTranz;
    using ParaTransData;

    public class MpTransSheetDao
    {
        /// <summary>
        /// CSV形式の翻訳シートを読み込み、言語データファイルに言語データ項目を反映する。
        /// https://doc-10-4s-sheets.googleusercontent.com/export/l9db5ndobdejfvmhepbkp2nf00/qe17rdk4p7jnb07dt9obh2gtso/1634388690000/109731952084879266506/106609323853708196418/149c7Bm7bCQAU_yR1ND7AXM2dj_NeSD7N3SfoXNHRkZE?format=csv&id=149c7Bm7bCQAU_yR1ND7AXM2dj_NeSD7N3SfoXNHRkZE&gid=1023120781&dat=AFCstmq92zWUIO9oC_YlrWgxmy-zFDmj0ZZkLGL8VvI3KYlMblSyj-ZSnrovwzxNlQKbin02appMg2olEdtJO9fnT_OmA570X7zHSFZoqcuZWJEiWo2nQwAw2FIz6lI6eA3F8kJOSwA-Jw8E9rgTfjoYWAFB7cuQdzWg-QRqUL7tnWJU5reF1-K08TmX4zrM222zp82V71B5I3R6ULJkihBoKlR4zplhyTZ5i5ADx5Qu95T5aLk1--mfwrD-4c29p0UdVTvEF280kLzOYa0LujHhXY-q10-700exmOBII0E7KhFX5BvnUOtAXtONGaAQYcpa_q0eK0bRhe4SmGFuCainaY_avZwONYsLuAk7TU5ezYZ1yvsckeDWPXkadrI01Bw69Gi7PSr-SutaVMC-VSLglByYvJMyVP0-UlSbGoRs_AvFMyBoAjw0A1fu2pa_ohfG0_JkVoshLjeFmqplt0blpZzIXh4asxofcC-N5t3n5vjri4404rl86pmk-Bk84QI4lJgM3O1zz8DVBShVWxYFelftqbitnHfgT4b8AcHQwjalp37fsKw5Eb9-fQTgrMT8G92dZBJ_O6W-bfNmHzFVbZaWYLYxtLjCQSPp1SXF0S3TpbjkVRhm2_fvOoXAQi7_RnpsKftfQcjO1IU6Epe8H0wAqJYXbjhFEjQvxyGbCBp0OMNF2KU0WhtwLZMAItoToYlUk2TmTrY3AKAEraUXfmNdKOkL
        /// </summary>
        /// <param name="languageDataFile">言語データファイル</param>
        /// <param name="path">CSV形式の翻訳シートのパス</param>
        /// <param name="useMT">機械翻訳の使用有無</param>
        public static void LoadFromCsv(
            MpParaTranzDataFile languageDataFile, string path, bool useMT)
        {
            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                NewLine = Environment.NewLine,
                Delimiter = ",",
                HasHeaderRecord = true,
            };

            using (var reader = new StreamReader(path, Encoding.UTF8))
            using (var csv = new CsvReader(reader, config))
            {
                csv.Context.RegisterClassMap<CsvMapper>();
                var records = csv.GetRecords<CsvRecord>();

                int lineNo = 0;
                foreach (var record in records)
                {
                    if (lineNo < 4)
                    {
                        //// ヘッダーをスキップ
                    }
                    else if (record.Key.StartsWith("0x00"))
                    {
                        //// 最終行をスキップする。
                    }
                    else
                    {
                        var item = new TrParaTranzDataWidthStatusSchema();
                        item.Key = record.Key;
                        item.Original = ChangeTags(record.OriginalText);
                        item.Translation = ChangeTags(record.TranslatedText);

                        languageDataFile.AddItem(item);
                    }

                    lineNo++;
                }
            }
        }

        /// <summary>
        /// 言語ファイルをCSV形式で保存する。
        /// </summary>
        /// <param name="languageDataFile">言語ファイル</param>
        /// <param name="path">保存するファイルのパス</param>
        public static void SaveToFile(MpParaTranzDataFile languageDataFile, string path)
        {
            using (var writer = new CsvWriter(
                new StreamWriter(path, false, Encoding.UTF8), CultureInfo.InvariantCulture))
            {
                writer.Context.RegisterClassMap<CsvMapper>();
                writer.WriteHeader<CsvRecord>();
                writer.NextRecord();

                foreach (var languageDataItem in languageDataFile.Items.Values)
                {
                    var itemData = new CsvRecord();

                    itemData.Key = languageDataItem.Key;
                    itemData.OriginalText = languageDataItem.Original;
                    itemData.TranslatedText = languageDataItem.Translation;

                    writer.WriteRecord(itemData);
                    writer.NextRecord();
                }
            }
        }

        /// <summary>
        /// タグを制御文字に変換したテキストを返す。
        /// </summary>
        /// <param name="originalText">テキスト</param>
        /// <returns>タグを制御文字に変換したテキスト</returns>
        private static string ChangeTags(string originalText)
        {
            var result = originalText.Replace("[LF]", "\n");

            return result;
        }

        /// <summary>
        /// レコード：CSV書き込み時はこのクラスの定義順に出力される。
        /// </summary>
        public class CsvRecord
        {
            public string Key { get; set; }

            public string TextType { get; set; }

            public string OriginalText { get; set; }

            public string TranslatedText { get; set; }
        }

        /// <summary>
        /// 格納ルール ：マッピングルール(一行目を列名とした場合は列名で定義することができる。)
        /// </summary>
        public class CsvMapper : CsvHelper.Configuration.ClassMap<CsvRecord>
        {
            public CsvMapper()
            {
                // 出力時の列の順番は指定した順となる。
                this.Map(x => x.Key).Index(0);
                this.Map(x => x.OriginalText).Index(4);
                this.Map(x => x.TranslatedText).Index(8);
            }
        }
    }
}
