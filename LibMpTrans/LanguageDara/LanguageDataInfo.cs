﻿namespace LibMpTrans.LanguageDara
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class LanguageDataInfo
    {
        /// <summary>
        /// 言語データファイルの辞書
        /// キーは言語番号
        /// </summary>
        public Dictionary<int, LanguageDataFile> Items { get; } = new Dictionary<int, LanguageDataFile>();

        public LanguageDataFile GetLanguageFile(int key)
        {
            if (this.Items.ContainsKey(key))
            {
                return this.Items[key];
            }
            else
            {
                return null;
            }
        }

        public void AddFile(LanguageDataFile languageDataFile)
        {
            if (this.Items.ContainsKey(languageDataFile.LanguageNo))
            {
                var currentLanguageDataFile = this.Items[languageDataFile.LanguageNo];
                currentLanguageDataFile.MergeLanguageDataFile(languageDataFile);
            }
            else
            {
                this.Items.Add(languageDataFile.LanguageNo, languageDataFile);
            }
        }

        public LanguageDataEntry GetEntry(int langNo, string key)
        {
            if (this.Items.ContainsKey(langNo))
            {
                var dataFile = this.Items[langNo];
                var entry = dataFile.GetEntry(key);
                return entry;
            }
            else
            {
                return null;
            }
        }

        public override string ToString()
        {
            var buff = new StringBuilder();

            var fileNo = 0;
            foreach (var languageDataFile in this.Items.Values)
            {
                buff.AppendLine($"FileNo({fileNo})---------------------------------");
                buff.Append($"{languageDataFile.ToString()}");
                fileNo++;
            }

            return buff.ToString();
        }
    }
}
