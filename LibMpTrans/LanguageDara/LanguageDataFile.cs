﻿namespace LibMpTrans.LanguageDara
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class LanguageDataFile
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="languageNo">言語番号</param>
        public LanguageDataFile(int languageNo)
        {
            this.LanguageNo = languageNo;
        }

        /// <summary>
        /// 言語データエントリーの辞書。
        /// キーは言語データエントリーの Key
        /// （キーは大文字小文字を区別する）
        /// </summary>
        public Dictionary<string, LanguageDataEntry> Items { get; } =
            new Dictionary<string, LanguageDataEntry>();

        /// <summary>
        /// 言語番号
        /// </summary>
        public int LanguageNo { get; } = 0;

        public void AddEntry(LanguageDataEntry languageDataEntry)
        {
            if (this.Items.ContainsKey(languageDataEntry.Key))
            {
                var msg = $"The key already exists. Key({languageDataEntry.Key})";
                throw new Exception(msg);
            }
            else
            {
                this.Items.Add(languageDataEntry.Key, languageDataEntry);
            }
        }

        public void MergeLanguageDataFile(LanguageDataFile languageDataFile)
        {
            foreach (var languageDataEntry in languageDataFile.Items.Values)
            {
                this.AddEntry(languageDataEntry);
            }
        }

        public LanguageDataEntry GetEntry(string key)
        {
            if (this.Items.ContainsKey(key))
            {
                return this.Items[key];
            }
            else
            {
                return null;
            }
        }

        public override string ToString()
        {
            var buff = new StringBuilder();

            var entryNo = 0;
            foreach (var entry in this.Items.Values)
            {
                buff.Append($"{entryNo}: {entry.ToString()}");
                entryNo++;
            }

            return buff.ToString();
        }
    }
}
