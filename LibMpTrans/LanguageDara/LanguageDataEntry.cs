﻿namespace LibMpTrans.LanguageDara
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// 言語データエントリー
    /// </summary>
    public class LanguageDataEntry
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="key">キー</param>
        /// <param name="text">原文</param>
        /// <param name="translatedText">翻訳文</param>
        /// <param name="context">文脈情報</param>
        public LanguageDataEntry(string key, string text, string context)
        {
            this.Key = key;
            this.Text = text;
            this.Context = context;
        }

        /// <summary>
        /// キー
        /// </summary>
        public string Key { get; }

        /// <summary>
        /// テキスト
        /// </summary>
        public string Text { get; }

        /// <summary>
        /// コンテキスト情報
        /// </summary>
        public string Context { get; }

        /// <summary>
        /// No break space をタグに変換したテキストを返す。
        /// </summary>
        /// <param name="text">テキスト</param>
        /// <returns>タグに変換したテキスト</returns>
        public static string ChangeNobreakSpaceToTag(string text)
        {
            return text.Replace("\u200b", "<nbsp>");
        }

        public override string ToString()
        {
            var buff = new StringBuilder();

            buff.AppendLine(
                $"Key({this.Key}) " +
                $"Original({this.Text}) " +
                $"Context({this.Context})");

            return buff.ToString();
        }
    }
}
