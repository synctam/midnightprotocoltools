﻿namespace MpParaTranzCli
{
    using System;
    using System.IO;
    using LibMpAssetUtils.Exporter;
    using LibMpTrans.ExcelSheets;
    using LibMpTrans.LanguageDara;
    using LibMpTrans.ParaTranz;
    using LibMpTrans.ResourceObjects;
    using LibMpTrans.TranslationSheets;
    using MonoOptions;
    using ParaTransData;
    using S5mDebugTools;

    internal class Program
    {
        private static int Main(string[] args)
        {
            //// --makemod -l 4 -i data\en\resources_00001.-86 -o data\jp\resources_00001.-86 -p data\ParaTranz\raw\MpParaTrans_vanilla.json.json --usenbsp -r
            //// --download --url https://paratranz.cn/api/projects/3302/artifacts/download -f data\ParaTranz
            //// --makesheet -l 2 -i data\jp\resources_00001.-86 -x _MpTransSheet(beta).xlsx -r
            //// --makesheet -l 2 -i data\jp\resources_00001.-86 -m data\excel\Midnight_Protocol_Localization_External_02.xlsx -x _MpTransSheet(beta).xlsx -r
            //// --update -i data\en\resources_00001.-86 -p data\ParaTranzMentenance\updated\MpParaTrans_vanilla.json --usenbsp -r
            //// --export -i data\en\resources.assets -o data\en\resources_00001.-86 --tpk data\config\classdata.tpk --asset I2Languages -r
            TOptions opt = new TOptions(args);
            if (opt.IsError)
            {
                TDebugUtils.Pause();
                return 1;
            }

            if (opt.Arges.Help)
            {
                opt.ShowUsage();

                TDebugUtils.Pause();
                return 1;
            }

            try
            {
                if (opt.Arges.CommandMakeMod)
                {
                    MakeMod(opt.Arges);
                }
                else if (opt.Arges.CommandParaTranzDownload)
                {
                    DownloadParaTranzData(opt.Arges);
                }
                else if (opt.Arges.CommandUpdated)
                {
                    MakeUpdates(opt.Arges);
                }
                else if (opt.Arges.CommandMakeSheet)
                {
                    MakeExcelSheet(opt.Arges);
                }
                else if (opt.Arges.CommandExport)
                {
                    ExportMonobehaviour(opt.Arges);
                }

                ////MakeApplyTrans();

                TDebugUtils.Pause();
                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                TDebugUtils.Pause();
                return 1;
            }
        }

        private static void ExportMonobehaviour(TOptions.TArgs opt)
        {
            var tpkPath = opt.FileNameClassPackage;
            var resourcePath = opt.FileNameSourceResourcePath;
            var assetExporter = new UnityAssetExporter(tpkPath, resourcePath);
            assetExporter.ExportMonoBehaviour(opt.AssetName, opt.FileNameTargetResourcePath);
        }

        private static void MakeExcelSheet(TOptions.TArgs opt)
        {
            var languageDataInfo = new LanguageDataInfo();
            Console.WriteLine($"Loading a resource file({opt.FileNameSourceResourcePath})...");
            MpResourceDao.LoadFromFile(languageDataInfo, opt.FileNameSourceResourcePath);
            Console.WriteLine($"Saving an Excel file({opt.FileNameExcelSheetPath})...");
            MpExcelSheetDao.SaveToFile(
                languageDataInfo, opt.FileNameMasterSheetPath, opt.FileNameExcelSheetPath, opt.LanguageNumber);
        }

        private static bool DownloadParaTranzData(TOptions.TArgs opt)
        {
            var downloader = new ParaTranzDownloader();
            Uri uri = new Uri(opt.ParaTranzDownloadUrl);
            downloader.Downlaod(uri, opt.FileNameParaTranzDownloadFolderPath);

            return true;
        }

        private static void MakeMod(TOptions.TArgs opt)
        {
            var paraTransFile = new MpParaTranzDataFile();
            MpParaTranzDao.LoadFromFile(paraTransFile, opt.FileNameParaTranzPath);
            ////File.WriteAllText(@"_debug.trans.txt", paraTransFile.ToString());

            var sourceResouce = opt.FileNameSourceResourcePath;
            var targetResource = opt.FileNameTargetResourcePath;

            MpResourceDao.SaveToFile(paraTransFile, sourceResouce, targetResource, opt.LanguageNumber, opt.UseNobreakSpace);
        }

        /// <summary>
        /// GoogleスプレッドシートのCSVファイルから翻訳データを抽出し、
        /// ParaTranz に編訳データを反映する JSON ファイルを作成する。
        /// </summary>
        private static void MakeApplyTrans()
        {
            var paraTransFile = new MpParaTranzDataFile();
            MpTransSheetDao.LoadFromCsv(paraTransFile, @"data\csv\MP - resources_00001.-86.csv", false);

            var jsonString = TrParaTranzRawSerialize.ToJson(paraTransFile.ToTranslatedList());
            File.WriteAllText(@"data\json\translated\MpParaTrans_vanilla.json", jsonString);
        }

        /// <summary>
        /// リソースファイルを読み込み、ParaTranz用の JSON ファイルを作成する。
        /// </summary>
        private static void MakeUpdates(TOptions.TArgs opt)
        {
            ////{
            ////    var paraTransFile = new MpParaTranzDataFile();
            ////    MpResourceDao.LoadFromFile(paraTransFile, @"data\raw\demo\resources_00001.-63", 0);
            ////    var jsonString = TrParaTranzRawSerialize.ToJson(paraTransFile.ToList());
            ////    File.WriteAllText(@"data\json\updated\MpParaTrans_demo.json", jsonString);
            ////}
            {
                var languageDataInfo = new LanguageDataInfo();
                MpResourceDao.LoadFromFile(languageDataInfo, opt.FileNameSourceResourcePath);
                File.WriteAllText(@"_debug_languageDataInfo.txt", languageDataInfo.ToString());
                TrParaTranzDataWidthStatusSchema.SaveToFile(
                    languageDataInfo, opt.FileNameParaTranzPath, 0, opt.UseNobreakSpace);
            }
        }
    }
}
