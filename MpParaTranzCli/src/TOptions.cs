﻿// ******************************************************************************
// Copyright (c) 2015-2019 synctam
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

namespace MonoOptions
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Mono.Options;

    /// <summary>
    /// コマンドライン オプション
    /// </summary>
    public class TOptions
    {
        //// ******************************************************************************
        //// Property fields
        //// ******************************************************************************
        private TArgs args;
        private bool isError = false;
        private StringWriter errorMessage = new StringWriter();
        private OptionSet optionSet;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="arges">コマンドライン引数</param>
        public TOptions(string[] arges)
        {
            this.args = new TArgs();
            this.Settings(arges);
            if (this.IsError)
            {
                this.ShowErrorMessage();
                this.ShowUsage();
            }
            else
            {
                this.CheckOption();
                if (this.IsError)
                {
                    this.ShowErrorMessage();
                    this.ShowUsage();
                }
                else
                {
                    //// skip
                }
            }
        }

        //// ******************************************************************************
        //// Property
        //// ******************************************************************************

        /// <summary>
        /// コマンドライン オプション
        /// </summary>
        public TArgs Arges { get { return this.args; } }

        /// <summary>
        /// コマンドライン オプションのエラー有無
        /// </summary>
        public bool IsError { get { return this.isError; } }

        /// <summary>
        /// エラーメッセージ
        /// </summary>
        public string ErrorMessage { get { return this.errorMessage.ToString(); } }

        /// <summary>
        /// Uasgeを表示する
        /// </summary>
        public void ShowUsage()
        {
            TextWriter writer = Console.Error;
            this.ShowUsage(writer);
        }

        /// <summary>
        /// Uasgeを表示する
        /// </summary>
        /// <param name="textWriter">出力先</param>
        public void ShowUsage(TextWriter textWriter)
        {
            var msg = new StringWriter();

            string exeName = Path.GetFileNameWithoutExtension(Environment.GetCommandLineArgs()[0]);
            msg.WriteLine(string.Empty);
            msg.WriteLine($@"使い方：");
            msg.WriteLine($@"ＭＯＤを作成する。");
            msg.WriteLine($@"  usage: {exeName} --makemod -i <入力リソースのパス> -o <出力リソースのパス> -p <ParaTranzのJSONファイルのパス> [-b] [-r]");
            msg.WriteLine($@"翻訳データをダウンロードする。");
            msg.WriteLine($@"  usage: {exeName} --download [--url <download url>] [-f <download folder>]");
            msg.WriteLine($@"Excel形式の翻訳シートを作成する。");
            msg.WriteLine($@"  usage: {exeName} --makesheet -l <langno> -i <リソースファイルのパス> -m <マスターシートのパス> -x <Excelファイルのパス> [-r]");
            msg.WriteLine($@"指定した PathID(--pathid)のリソースをエクスポートする。");
            msg.WriteLine($@"  usage: {exeName} --export -i <リソースファイルのパス> -o <出力リソースのパス> --tpk <class package> --pathid <path id> [-r]");
            msg.WriteLine($@"OPTIONS:");
            this.optionSet.WriteOptionDescriptions(msg);
            msg.WriteLine($@"Example:");
            msg.WriteLine($@"  オリジナルのリソースファイル(-i)とParaTranzの翻訳データ(-p)から翻訳済みリソースファイル(-o)を言語番号'4'に作成する。");
            msg.WriteLine($@"    {exeName} --makemod -i data\en\resources_00001.-86 -o data\jp\resources_00001.-86 -p data\ParaTranz\raw\MpParaTrans_vanilla.json.json -l 4");
            msg.WriteLine($@"終了コード:");
            msg.WriteLine($@" 0  正常終了");
            msg.WriteLine($@" 1  異常終了");
            msg.WriteLine();

            if (textWriter == null)
            {
                textWriter = Console.Error;
            }

            textWriter.Write(msg.ToString());
        }

        /// <summary>
        /// エラーメッセージ表示
        /// </summary>
        public void ShowErrorMessage()
        {
            TextWriter writer = Console.Error;
            this.ShowErrorMessage(writer);
        }

        /// <summary>
        /// エラーメッセージ表示
        /// </summary>
        /// <param name="textWriter">出力先</param>
        public void ShowErrorMessage(TextWriter textWriter)
        {
            if (textWriter == null)
            {
                textWriter = Console.Error;
            }

            textWriter.Write(this.ErrorMessage);
        }

        /// <summary>
        /// オプション文字の設定
        /// </summary>
        /// <param name="args">args</param>
        private void Settings(string[] args)
        {
            this.optionSet = new OptionSet()
            {
                { "makemod"   , this.args.CommandMakeModText         , v => this.args.CommandMakeMod  = v != null},
                { "update"    , this.args.CommandUpdatedText         , v => this.args.CommandUpdated  = v != null},
                { "download"  , this.args.CommandParaTranzDownloadText , v => this.args.CommandParaTranzDownload  = v != null},
                { "makesheet" , this.args.CommandMakeSheetText       , v => this.args.CommandMakeSheet            = v != null},
                { "export"    , this.args.CommandExportText          , v => this.args.CommandExport               = v != null},
                { "i|in="     , this.args.FileNameSourceResourceText , v => this.args.FileNameSourceResourcePath = v},
                { "m|master=" , this.args.FileNameMasterSheetText    , v => this.args.FileNameMasterSheetPath    = v},
                { "o|out="    , this.args.FileNameTargetResourceText , v => this.args.FileNameTargetResourcePath = v},
                { "p|para="   , this.args.FileNameRapaTranzText      , v => this.args.FileNameParaTranzPath      = v},
                { "l|langno=" , this.args.LangNoText                 , v => this.args.LangNo                     = v},
                { "b|usenbsp" , this.args.UseNobreakSpaceText        , v => this.args.UseNobreakSpace    = v != null},
                { "asset="    , this.args.AssetNameText              , v => this.args.AssetName                  = v},
                { "tpk="      , this.args.FileNameClassPackageText   , v => this.args.FileNameClassPackage       = v},
                { "url="      , this.args.ParaTranzUrlText           , v => this.args.ParaTranzDownloadUrl       = v},
                { "f|folder=" , this.args.FileNameParaTranzDownloadFolderText , v => this.args.FileNameParaTranzDownloadFolderPath = v},
                { "x|excel="  , this.args.FileNameExcelSheetPathText , v => this.args.FileNameExcelSheetPath     = v},
                { "r"         , this.args.UseReplaceText             , v => this.args.UseReplace      = v != null},
                { "h|help"    , "ヘルプ"                             , v => this.args.Help            = v != null},
            };

            List<string> extra;
            try
            {
                extra = this.optionSet.Parse(args);
                if (extra.Count > 0)
                {
                    // 指定されたオプション以外のオプションが指定されていた場合、
                    // extra に格納される。
                    // 不明なオプションが指定された。
                    this.SetErrorMessage("エラー：不明なオプションが指定されました。");
                    extra.ForEach(t => this.SetErrorMessage(t));
                    this.isError = true;
                }
            }
            catch (OptionException e)
            {
                ////パースに失敗した場合OptionExceptionを発生させる
                this.SetErrorMessage(e.Message);
                this.isError = true;
            }
        }

        /// <summary>
        /// オプションのチェック
        /// </summary>
        private void CheckOption()
        {
            //// -h
            if (this.Arges.Help)
            {
                this.SetErrorMessage();
                this.isError = false;
                return;
            }

            if (this.IsErrorCommand())
            {
                return;
            }
            else
            {
                if (this.Arges.CommandMakeMod)
                {
                    if (this.IsErrorSourceResourceFile())
                    {
                        return;
                    }

                    if (this.IsErrorTargetResourceFile())
                    {
                        return;
                    }

                    if (this.IsErrorParaTranzJsonInputFile())
                    {
                        return;
                    }

                    if (this.IsErrorLangNo())
                    {
                        return;
                    }

                    if (this.IsErrorUseNobreakSpace())
                    {
                        return;
                    }
                }
                else if (this.Arges.CommandUpdated)
                {
                    if (this.IsErrorSourceResourceFile())
                    {
                        return;
                    }

                    if (this.IsErrorParaTranzJsonOutputFile())
                    {
                        return;
                    }
                }
                else if (this.Arges.CommandParaTranzDownload)
                {
                    if (this.IsErrorParaTranzDownloadUri())
                    {
                        return;
                    }

                    if (this.IsErrorParaTranzDownloadFolder())
                    {
                        return;
                    }
                }
                else if (this.Arges.CommandMakeSheet)
                {
                    if (this.IsErrorLangNo())
                    {
                        return;
                    }

                    if (this.IsErrorSourceResourceFile())
                    {
                        return;
                    }

                    if (this.IsErrorMasterSheetFile())
                    {
                        return;
                    }

                    if (this.IsErrorExcelSheetFile())
                    {
                        return;
                    }
                }
                else if (this.Arges.CommandExport)
                {
                    if (this.IsErrorAssetName())
                    {
                        return;
                    }

                    if (this.IsErrorPathClassPackage())
                    {
                        return;
                    }

                    if (this.IsErrorSourceResourceFile())
                    {
                        return;
                    }

                    if (this.IsErrorTargetResourceFile())
                    {
                        return;
                    }
                }
            }

            this.isError = false;
            return;
        }

        private bool IsErrorPathClassPackage()
        {
            if (string.IsNullOrWhiteSpace(this.Arges.FileNameClassPackage))
            {
                this.SetErrorMessage($@"エラー：(--tpk)クラスパッケージファイルのパスを指定してください。");
                this.isError = true;

                return true;
            }
            else
            {
                if (!File.Exists(this.Arges.FileNameClassPackage))
                {
                    this.SetErrorMessage(
                        $"エラー：(--tpk)クラスパッケージファイルが存在しません。{Environment.NewLine}" +
                        $"({Path.GetFullPath(this.Arges.FileNameClassPackage)})");
                    this.isError = true;

                    return true;
                }
            }

            return false;
        }

        private bool IsErrorAssetName()
        {
            if (string.IsNullOrWhiteSpace(this.Arges.AssetName))
            {
                var msg =
                    $"エクスポートするアセット名（--asset）を指定してください。{Environment.NewLine}" +
                    $"PathID({this.Arges.AssetName})";
                this.SetErrorMessage(msg);
                this.isError = true;
                return true;
            }

            return false;
        }

        private bool IsErrorParaTranzDownloadFolder()
        {
            var invalidChars = Path.GetInvalidPathChars();

            if (this.Arges.FileNameParaTranzDownloadFolderPath.IndexOfAny(invalidChars) < 0)
            {
                //// OK: フォルダー名は正しい
            }
            else
            {
                var msg =
                    $"フォルダー名(-f)に使用できない文字が使われています。{Environment.NewLine}" +
                    $"path({this.Arges.FileNameParaTranzDownloadFolderPath})";
                this.SetErrorMessage(msg);
                this.isError = true;
                return true;
            }

            return false;
        }

        private bool IsErrorMasterSheetFile()
        {
            if (string.IsNullOrWhiteSpace(this.Arges.FileNameMasterSheetPath))
            {
                this.SetErrorMessage($@"エラー：(-m)マスターシートのパスを指定してください。");
                this.isError = true;

                return true;
            }
            else
            {
                if (!File.Exists(this.Arges.FileNameMasterSheetPath))
                {
                    this.SetErrorMessage(
                        $"エラー：(-m)マスターシートが存在しません。{Environment.NewLine}" +
                        $"({Path.GetFullPath(this.Arges.FileNameMasterSheetPath)})");
                    this.isError = true;

                    return true;
                }
            }

            return false;
        }

        private bool IsErrorExcelSheetFile()
        {
            var invalidChars = Path.GetInvalidPathChars();
            if (string.IsNullOrWhiteSpace(this.Arges.FileNameExcelSheetPath))
            {
                this.SetErrorMessage($@"エラー：(-x)Excelファイルのパスを指定してください。");
                this.isError = true;

                return true;
            }
            else if (this.Arges.FileNameExcelSheetPath.IndexOfAny(invalidChars) < 0)
            {
                //// OK: ファイル名は正しい
                if (File.Exists(this.Arges.FileNameExcelSheetPath))
                {
                    //// 出力ファイルはすでに存在する
                    if (this.Arges.UseReplace)
                    {
                        //// OK: 上書き指示有り
                    }
                    else
                    {
                        this.SetErrorMessage(
                            $@"エラー：(-x)Excelファイルが既に存在します。{Environment.NewLine}" +
                            $@"({Path.GetFullPath(this.Arges.FileNameExcelSheetPath)}){Environment.NewLine}" +
                            $@"上書きする場合は '-r' オプションを指定してください。");
                        this.isError = true;

                        return true;
                    }
                }
                else
                {
                    //// OK: 新規作成
                }
            }
            else
            {
                var msg =
                    $"ファイル名(-x)に使用できない文字が使われています。{Environment.NewLine}" +
                    $"path({this.Arges.FileNameExcelSheetPath})";
                this.SetErrorMessage(msg);
                this.isError = true;
                return true;
            }

            return false;
        }

        private bool IsErrorParaTranzDownloadUri()
        {
            if (string.IsNullOrWhiteSpace(this.Arges.ParaTranzDownloadUrl))
            {
                //// OK: 未指定の場合はダウンロードしない。
            }
            else if (!Uri.IsWellFormedUriString(this.Arges.ParaTranzDownloadUrl, UriKind.Absolute))
            {
                var msg =
                    $"ParaTranzのダウンロードＵＲＬ(-u)に誤りがあります。{Environment.NewLine}" +
                    $"URL({this.Arges.ParaTranzDownloadUrl})";
                this.SetErrorMessage(msg);
                this.isError = true;
                return true;
            }

            return false;
        }

        private bool IsErrorCommand()
        {
            if (this.Arges.CommandMakeMod)
            {
                return false;
            }
            else if (this.Arges.CommandParaTranzDownload)
            {
                return false;
            }
            else if (this.Arges.CommandUpdated)
            {
                return false;
            }
            else if (this.Arges.CommandMakeSheet)
            {
                return false;
            }
            else if (this.Arges.CommandExport)
            {
                return false;
            }
            else
            {
                this.SetErrorMessage($@"エラー：コマンド(--makemod, --download, --makesheet --update または --export)を指定してください。");
                this.isError = true;
                return true;
            }
        }

        private bool IsErrorLangNo()
        {
            int langNo = 0;

            if (int.TryParse(this.Arges.LangNo, out langNo))
            {
                this.Arges.LanguageNumber = langNo;
                return false;
            }

            return true;
        }

        private bool IsErrorSourceResourceFile()
        {
            if (string.IsNullOrWhiteSpace(this.Arges.FileNameSourceResourcePath))
            {
                this.SetErrorMessage($@"エラー：(-i)オリジナル版のリソースファイルのパスを指定してください。");
                this.isError = true;

                return true;
            }
            else
            {
                if (!File.Exists(this.Arges.FileNameSourceResourcePath))
                {
                    this.SetErrorMessage($@"エラー：(-i)オリジナル版のリソースファイルが存在しません。{Environment.NewLine}({Path.GetFullPath(this.Arges.FileNameSourceResourcePath)})");
                    this.isError = true;

                    return true;
                }
            }

            return false;
        }

        private bool IsErrorTargetResourceFile()
        {
            if (string.IsNullOrWhiteSpace(this.Arges.FileNameTargetResourcePath))
            {
                this.SetErrorMessage($@"エラー：(-o)出力リソースファイルのパスを指定してください。");
                this.isError = true;

                return true;
            }

            if (File.Exists(this.Arges.FileNameTargetResourcePath) && !this.args.UseReplace)
            {
                this.SetErrorMessage(
                    $@"エラー：(-o)出力リソースファイルが既に存在します。{Environment.NewLine}" +
                    $@"({Path.GetFullPath(this.Arges.FileNameTargetResourcePath)}){Environment.NewLine}" +
                    $@"上書きする場合は '-r' オプションを指定してください。");
                this.isError = true;

                return true;
            }

            return false;
        }

        private bool IsErrorParaTranzJsonInputFile()
        {
            if (string.IsNullOrWhiteSpace(this.Arges.FileNameParaTranzPath))
            {
                this.SetErrorMessage($@"エラー：(-p)ParaTranz(raw)のJSONファイルのパスを指定してください。");
                this.isError = true;

                return true;
            }

            return false;
        }

        private bool IsErrorParaTranzJsonOutputFile()
        {
            if (string.IsNullOrWhiteSpace(this.Arges.FileNameParaTranzPath))
            {
                this.SetErrorMessage($@"エラー：(-p)出力するJSONファイルのパスを指定してください。");
                this.isError = true;

                return true;
            }

            if (File.Exists(this.Arges.FileNameParaTranzPath) && !this.args.UseReplace)
            {
                this.SetErrorMessage(
                    $@"エラー：(-p)出力するJSONファイルが既に存在します。{Environment.NewLine}" +
                    $@"({Path.GetFullPath(this.Arges.FileNameParaTranzPath)}){Environment.NewLine}" +
                    $@"上書きする場合は '-r' オプションを指定してください。");
                this.isError = true;

                return true;
            }

            return false;
        }

        private bool IsErrorUseNobreakSpace()
        {
            return false;
        }

        private void SetErrorMessage(string errorMessage = null)
        {
            if (errorMessage != null)
            {
                this.errorMessage.WriteLine(errorMessage);
            }
        }

        /// <summary>
        /// オプション項目
        /// </summary>
        public class TArgs
        {
            public bool CommandMakeMod { get; internal set; }

            public string CommandMakeModText { get; internal set; } =
                $"[コマンド] 翻訳済みリソースを作成する。";

            public bool CommandUpdated { get; internal set; }

            public string CommandUpdatedText { get; internal set; } =
                $"[コマンド] ParaTranz更新用のデータを作成する。";

            public bool CommandParaTranzDownload { get; internal set; }

            public string CommandParaTranzDownloadText { get; internal set; } =
                $"[コマンド] ParaTranzから翻訳データをダウンロードする。";

            public bool CommandMakeSheet { get; internal set; }

            public string CommandMakeSheetText { get; internal set; } =
                $"[コマンド] 翻訳済みリソースファイルからExcelシートを作成する（ベータ版です）。";

            public bool CommandExport { get; internal set; }

            public string CommandExportText { get; internal set; } =
                $"[コマンド] リソースをエクスポートする（ベータ版です）。";

            public string FileNameSourceResourcePath { get; internal set; }

            public string FileNameSourceResourceText { get; internal set; } =
                "オリジナル版のリソースファイルのパスを指定する。";

            public string FileNameTargetResourcePath { get; internal set; }

            public string FileNameTargetResourceText { get; internal set; } =
                $"出力するリソースファイルのパスを指定する。";

            public string FileNameParaTranzPath { get; internal set; }

            public string FileNameRapaTranzText { get; internal set; } =
                $"ParaTranzのJSONファイルのパスを指定する。";

            public string FileNameClassPackage { get; internal set; }

            public string FileNameClassPackageText { get; internal set; } =
                $"クラスパッケージファイルのパス";

            public string FileNameMasterSheetPath { get; internal set; }

            public string FileNameMasterSheetText { get; internal set; } =
                $"マスターシートのパス";

            public string LangNo { get; internal set; }

            public int LanguageNumber { get; internal set; }

            public string LangNoText { get; internal set; } =
                $"出力する言語の番号を指定する。{Environment.NewLine}(en:0, nl:1, de:2, ru:3, zh-CN:4)";

            public string ParaTranzDownloadUrl { get; internal set; }

            public string ParaTranzUrlText { get; internal set; }

            public string FileNameParaTranzDownloadFolderPath { get; internal set; }

            public string FileNameParaTranzDownloadFolderText { get; internal set; } =
                $"ダウンロードフォルダーのパスを指定する。";

            public string FileNameExcelSheetPath { get; internal set; }

            public string FileNameExcelSheetPathText { get; internal set; } =
                $"Excel形式の翻訳シートのパスを指定する。";

            public string AssetName { get; internal set; }

            public string AssetNameText { get; internal set; } =
                $"エクスポートするアセット名を指定する。";

            public bool UseNobreakSpace { get; internal set; }

            public string UseNobreakSpaceText { get; internal set; } =
                $"ノーブレークスペースの使用有無。使用しない場合は通常の半角スペースを使用されます。";

            public bool UseReplace { get; internal set; }

            public string UseReplaceText { get; internal set; } =
                $"出力するファイルが既に存在する場合はを上書きする。";

            public bool Help { get; set; }
        }
    }
}
