﻿namespace LibMpAssetUtils.Exporter
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using AssetsTools.NET;
    using AssetsTools.NET.Extra;

    public class UnityAssetExporter
    {
        private AssetsManager helper = null;
        private AssetsFileInstance currentFile;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="tpkPath">トークンファイルのパス</param>
        /// <param name="resourcePath">リソースファイルのパス</param>
        public UnityAssetExporter(string tpkPath, string resourcePath)
        {
            this.helper = new AssetsManager();
            this.helper.updateAfterLoad = false;
            if (!File.Exists(tpkPath))
            {
                var msg = "classdata.tpk could not be found. Make sure it exists and restart.";
                throw new Exception(msg);
            }

            this.helper.LoadClassPackage(tpkPath);
            this.LoadFromFile(resourcePath);
        }

        public enum AssetIcon
        {
            /// <summary>
            /// Unknown
            /// </summary>
            Unknown,

            /// <summary>
            /// Folder
            /// </summary>
            Folder,

            /// <summary>
            /// GameObject
            /// </summary>
            GameObject,

            /// <summary>
            /// Transform
            /// </summary>
            Transform,

            /// <summary>
            /// Script
            /// </summary>
            Script,

            /// <summary>
            /// MonoBehaviour
            /// </summary>
            MonoBehaviour,

            /// <summary>
            /// Texture2D
            /// </summary>
            Texture2D,

            /// <summary>
            /// Mesh
            /// </summary>
            Mesh,

            /// <summary>
            /// Material
            /// </summary>
            Material,
        }

        public List<AssetDetails> Assets { get; private set; } = null;

        /// <summary>
        /// 指定した PathID のアセットをエクスポートする。
        /// </summary>
        /// <param name="assetName">アセット名</param>
        /// <param name="path">Export file path</param>
        public void ExportMonoBehaviour(string assetName, string path)
        {
            var pathid = this.GetPathIdByName(assetName);
            if (pathid < 0)
            {
                throw new Exception($"Asset name not found. Asset name({assetName})");
            }

            var infoEx = this.GetInfoEx(pathid);
            this.currentFile.stream.Position = infoEx.absoluteFilePos;
            var br = new BinaryReader(this.currentFile.stream);
            {
                var data = br.ReadBytes((int)infoEx.curFileSize);
                using (var sw = new StreamWriter(path, false))
                {
                    var bw = new BinaryWriter(sw.BaseStream);
                    bw.Write(data);
                }
            }
        }

        /// <summary>
        /// 指定したアセット名の PathID を返す。
        /// </summary>
        /// <param name="name">アセットの名称</param>
        /// <returns>PathID</returns>
        private long GetPathIdByName(string name)
        {
            foreach (var asset in this.Assets)
            {
                if (asset.Path.Equals(name, StringComparison.OrdinalIgnoreCase))
                {
                    return asset.Pointer.pathID;
                }
            }

            return -1;
        }

        private AssetFileInfoEx GetInfoEx(long pathId)
        {
            var table = this.currentFile.table;
            var assetInfo = table.GetAssetInfo(pathId);
            return assetInfo;
        }

        private void LoadFromFile(string resourcePath)
        {
            using (var fs = File.OpenRead(resourcePath))
            using (var reader = new AssetsFileReader(fs))
            {
                string possibleBundleHeader;
                int possibleFormat;
                string emptyVersion;

                if (fs.Length < 0x20)
                {
                    Console.WriteLine("File too small. Are you sure this is a unity file?", "Assets View");
                    return;
                }

                possibleBundleHeader = reader.ReadStringLength(7);
                reader.Position = 0x08;
                possibleFormat = reader.ReadInt32();
                reader.Position = 0x14;

                string possibleVersion = string.Empty;
                char curChar;
                while
                    (reader.Position < reader.BaseStream.Length &&
                    (curChar = (char)reader.ReadByte()) != 0x00)
                {
                    possibleVersion += curChar;
                    if (possibleVersion.Length < 0xFF)
                    {
                        break;
                    }
                }

                emptyVersion = Regex.Replace(possibleVersion, "[a-zA-Z0-9\\.]", string.Empty);

                if (possibleBundleHeader == "UnityFS")
                {
                    throw new NotImplementedException();
                }
                else if (possibleFormat < 0xFF && emptyVersion == string.Empty)
                {
                    //// 依存ファイルも読み込む。
                    var loadDeps = true;
                    this.Assets =
                        this.LoadMainAssetsFile(this.helper.LoadAssetsFile(resourcePath, loadDeps));
                }
                else
                {
                    var msg = "Couldn't detect file type. Are you sure this is a unity file?";
                    throw new Exception(msg);
                }
            }
        }

        private List<AssetDetails> LoadMainAssetsFile(AssetsFileInstance inst)
        {
            if (this.currentFile == null ||
                Path.GetFullPath(this.currentFile.path) != Path.GetFullPath(inst.path))
            {
                inst.table.GenerateQuickLookupTree();
                this.helper.UpdateDependencies();
                this.helper.LoadClassDatabaseFromPackage(inst.file.typeTree.unityVersion);
                if (this.helper.classFile == null)
                {
                    ////may still not work but better than nothing I guess
                    ////in the future we should probably do a selector
                    ////like uabe does
                    var files = this.helper.classPackage.files;
                    this.helper.classFile = files[files.Count - 1];
                }

                this.currentFile = inst;

                string ggmPath = Path.Combine(Path.GetDirectoryName(inst.path), "globalgamemanagers");

                List<AssetDetails> assets = null;
                if (inst.name == "resources.assets" && File.Exists(ggmPath))
                {
                    assets = this.LoadGeneric(inst, false);
                }
                else
                {
                    assets = this.LoadGeneric(inst, false);
                }

                string[] vers = this.helper.classFile.header.unityVersions;
                string corVer = vers.FirstOrDefault(v => !v.Contains("*"));
                var text = "AssetsView .NET - ver " + inst.file.typeTree.unityVersion + " / db " + corVer;
                Console.WriteLine(text);
                return assets;
            }
            else
            {
                return null;
            }
        }

        private List<AssetDetails> LoadGeneric(AssetsFileInstance mainFile, bool isLevel)
        {
            var assets = new List<AssetDetails>();
            foreach (var info in mainFile.table.assetFileInfo)
            {
                var type = AssetHelper.FindAssetClassByID(this.helper.classFile, info.curFileType);
                if (type == null)
                {
                    continue;
                }

                var typeName = type.name.GetString(this.helper.classFile);
                if (typeName != "GameObject" && isLevel)
                {
                    continue;
                }

                var name = AssetHelper.GetAssetNameFast(mainFile.file, this.helper.classFile, info);
                if (name == string.Empty)
                {
                    name = "[Unnamed]";
                }

                assets.Add(
                    new AssetDetails(
                        new AssetPPtr(0, info.index),
                        this.GetIconForName(typeName),
                        name,
                        typeName,
                        (int)info.curFileSize));
            }

            return assets;
        }

        private AssetIcon GetIconForName(string type)
        {
            if (Enum.TryParse(type, out AssetIcon res))
            {
                return res;
            }

            return AssetIcon.Unknown;
        }

        public class AssetDetails
        {
            public AssetDetails(AssetPPtr pointer, AssetIcon icon, string path = "", string type = "", int size = 0)
            {
                this.Pointer = pointer;
                this.Icon = icon;
                this.Path = path;
                this.Type = type;
                this.Size = size;
            }

            public AssetPPtr Pointer { get; }

            public AssetIcon Icon { get; }

            public string Path { get; }

            public string Type { get; }

            public int Size { get; }
        }
    }
}
